module Main where

import Interpreter (createInterpreter, runInterpreter)
import Optimizer (fuseOPs, multiplicationLoops, resetLoops)
import Parsing (createJmpMap, parseToIR)

import System.Environment (getArgs)
import System.Exit (exitFailure)

main :: IO ()
main = do
  arg <- mayHead <$> getArgs
  f <-
    case arg of
      Nothing -> help >> exitFailure
      Just f -> return f
  putStrLn "Parsing program..."
  programIR <- parseToIR <$> readFile f
  let prog' = multiplicationLoops . resetLoops . fuseOPs $ programIR
  let jmpMap = createJmpMap prog'
  putStrLn "Launching the interpreter..."
  let interpreter = createInterpreter prog' jmpMap
  (_, _) <- runInterpreter interpreter
  putStr "\nDone\n"
  where
    mayHead :: [a] -> Maybe a
    mayHead [] = Nothing
    mayHead (x:_) = Just x

help :: IO ()
help =
  putStrLn
    ("Error: You have to provide a file to interpret.\n" ++
     "Usage:\tbf-interpreter <file>")
