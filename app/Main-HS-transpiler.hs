{-# LANGUAGE LambdaCase #-}

module Main where

import MyTypes (BFInterRepr, OP_TYPE(..))
import Optimizer (fuseOPs, multiplicationLoops, resetLoops)
import Parsing (parseToIR)

import Data.List (intercalate)
import System.Environment (getArgs)
import System.Exit (exitFailure)

main :: IO ()
main = do
  args <- take 2 <$> getArgs
  [inFile, outFile] <-
    case length args of
      2 -> return [args !! 0, args !! 1]
      _ -> help >> exitFailure
  putStrLn "Parsing program..."
  programIR <- parseToIR <$> readFile inFile
  -- let prog' = multiplicationLoops . resetLoops . fuseOPs $ programIR
  let prog' = resetLoops . fuseOPs $ programIR
  putStrLn $ "Transpiling to:\t" ++ outFile
  let resultingC = transpile prog'
  writeFile outFile (unlines resultingC)
  putStr "\nDone\n"

help :: IO ()
help = putStrLn ("Usage:\n" ++ "\tbf2hs <input.bf> <transpiled.hs>")

transpile :: BFInterRepr -> [String]
transpile ir = concat [header, ["instructions = [ " ++ prog ++ " ]"], footer]
  where
    prog = cleanup $ concatMap opMap ir
    cleanup :: [Char] -> [Char]
    cleanup (x:y:z:rest) =
      if [x, y, z] == ", ]"
        then " ]" ++ cleanup rest
        else x : cleanup (y : z : rest)
    cleanup _Any = _Any

opMap :: OP_TYPE -> String
opMap =
  \case
    OP_INCR n -> "incr " ++ show n ++ ", "
    OP_DECR n -> "decr " ++ show n ++ ", "
    OP_RIGHT n -> "rOP " ++ show n ++ ", "
    OP_LEFT n -> "lOP " ++ show n ++ ", "
    OP_PRINT -> "pOP" ++ ", "
    OP_JMP_IF_NULL -> "jmpLoop [ "
    OP_JMP_BACK_IF_NON_NULL -> "]" ++ ", "
    OP_EXIT -> "const exitSuccess"
    OP_INPUT -> "gOP" ++ ", "
    OP_SET_TO_NULL -> "setNull , "
    OP_MULT (offset, factor) -> error "Mult not implemented"

tapeSize :: Integer
tapeSize = 32768

header :: [String]
header =
  [ "module Main where"
  , "import Data.Char (ord, chr)"
  , "import Control.Monad ((>=>))"
  , "import System.Exit (exitSuccess)"
  , "import Prelude hiding (until)"
  , "chain :: (Monad m) => [a -> m a] -> (a -> m a)"
  , "chain = foldr (>=>) return"
  , "until :: (Monad m) => (a -> Bool) -> [a -> m a] -> a -> m a"
  , "until cond ops x = if cond x then pure x else do"
  , "    x' <- chain ops x"
  , "    until cond ops x'"
  , "data Tape = Tape {_prev :: [Int], _cur :: Int, _next :: [Int]}"
  , "initT :: Tape"
  , "initT = Tape [] 0 (repeat 0)"
  , "incr :: Int -> Tape -> IO Tape"
  , "incr n t = pure $ t {_cur = _cur t + n}"
  , "decr :: Int -> Tape -> IO Tape"
  , "decr n t = pure $ t {_cur = _cur t - n}"
  -- , "mult :: Int -> Int -> Tape -> IO Tape"
  -- , "mult o f t = pure $ t {_cur = _cur t - n}"
  , "setNull :: Tape -> IO Tape"
  , "setNull t = pure $ t {_cur = 0}"
  , "pOP :: Tape -> IO Tape"
  , "pOP t = do"
  , "    putChar . chr $ _cur t"
  , "    return t"
  , "gOP :: Tape -> IO Tape"
  , "gOP t = do"
  , "    c <- getChar"
  , "    return $ t {_cur = ord c}"
  , "rOP :: Int -> Tape -> IO Tape"
  , "rOP n t = pure t'"
  , "    where"
  , "        (add, cur':next) = splitAt (n-1) (_next t)"
  , "        t' = Tape (reverse add ++ _cur t : _prev t) cur' next"
  , "lOP :: Int -> Tape -> IO Tape"
  , "lOP n t = pure t'"
  , "    where"
  , "        (xs',prev') = splitAt n $ _prev t"
  , "        (x:xs) = reverse xs'"
  , "        t' = Tape prev' x (xs ++ _cur t: _next t)"
  , "isNull :: Tape -> Bool"
  , "isNull t = 0 == _cur t"
  , "jmpLoop :: [Tape -> IO Tape] -> Tape -> IO Tape"
  , "jmpLoop = until isNull"
  , "bf :: Tape -> IO Tape"
  , "bf = chain instructions"
  , "instructions :: [Tape -> IO Tape]"
  ]

footer :: [String]
footer = ["main :: IO ()", "main = do", "    _ <- bf initT", "    return ()"]
