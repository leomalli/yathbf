{-# LANGUAGE LambdaCase #-}

module Main where

import MyTypes (BFInterRepr, OP_TYPE(..))
import Optimizer (fuseOPs, multiplicationLoops, resetLoops)
import Parsing (parseToIR)

import System.Environment (getArgs)
import System.Exit (exitFailure)

main :: IO ()
main = do
  args <- take 2 <$> getArgs
  [inFile, outFile] <-
    case length args of
      2 -> return [args !! 0, args !! 1]
      _ -> help >> exitFailure
  putStrLn "Parsing program..."
  programIR <- parseToIR <$> readFile inFile
  let prog' = multiplicationLoops . resetLoops . fuseOPs $ programIR
  putStrLn $ "Transpiling to:\t" ++ outFile
  let resultingC = transpile prog'
  writeFile outFile (unlines resultingC)
  putStr "\nDone\n"

help :: IO ()
help = putStrLn ("Usage:\n" ++ "\tbf2c <input.bf> <transpiled.c>")

transpile :: BFInterRepr -> [String]
transpile ir = concat [header, prog, footer]
  where
    prog = map opMap ir

opMap :: OP_TYPE -> String
opMap =
  \case
    OP_INCR n -> "(*pt) += " ++ show n ++ ";"
    OP_DECR n -> "(*pt) -= " ++ show n ++ ";"
    OP_RIGHT n -> "pt += " ++ show n ++ ";"
    OP_LEFT n -> "pt -= " ++ show n ++ ";"
    OP_PRINT -> "putchar(*pt);"
    OP_JMP_IF_NULL -> "while ((*pt) != 0){"
    OP_JMP_BACK_IF_NON_NULL -> "}"
    OP_EXIT -> "return 0;"
    OP_INPUT -> "(*pt) = getchar();"
    OP_SET_TO_NULL -> "(*pt) = 0;"
    OP_MULT (offset, factor) ->
      "pt[" ++ show offset ++ "] += pt[0] * " ++ show factor ++ ";"

tapeSize :: Integer
tapeSize = 32768

header :: [String]
header =
  [ "#include <stdio.h>"
  , "#include <stdint.h>"
  , "#define TS " ++ show tapeSize
  , "uint8_t tape[TS] = {0};"
  , "uint8_t *pt = tape;"
  , "int main(void){"
  ]

footer :: [String]
footer = ["return 0;}"]
