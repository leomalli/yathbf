{-# LANGUAGE LambdaCase #-}

module Main where

import MyTypes (BFInterRepr, OP_TYPE(..))
import Optimizer (fuseOPs, multiplicationLoops, resetLoops)
import Parsing (createJmpMap, parseToIR)

import Data.IntMap.Strict (IntMap, (!?))
import Data.Maybe (fromJust)
import System.Environment (getArgs)
import System.Exit (exitFailure)

main :: IO ()
main = do
  args <- take 2 <$> getArgs
  [inFile, outFile] <-
    case length args of
      2 -> return [args !! 0, args !! 1]
      _ -> help >> exitFailure
  putStrLn "Parsing program..."
  programIR <- parseToIR <$> readFile inFile
  let prog' = multiplicationLoops . resetLoops . fuseOPs $ programIR
  let jmpMap = createJmpMap prog'
  putStrLn $ "Transpiling to:\t" ++ outFile
  let resultingC = transpile prog' jmpMap
  writeFile outFile (unlines resultingC)
  putStr "\nDone\n"

help :: IO ()
help = putStrLn ("Usage:\n" ++ "\tbf2asm <input.bf> <transpiled.asm>")

transpile :: BFInterRepr -> IntMap Int -> [String]
transpile ir jmp = concat [header, prog, footer]
  where
    prog = concatMap (opMap jmp) $ zip ir [0 ..]

opMap :: IntMap Int -> (OP_TYPE, Int) -> [String]
opMap jmp =
  \case
    (OP_INCR n, _) ->
      map ("  " ++) ["; --- OP_INCR ---", "add byte [rbx], " ++ show n]
    (OP_DECR n, _) ->
      map ("  " ++) ["; --- OP_DECR --- ", "sub byte [rbx], " ++ show n]
    (OP_RIGHT n, _) ->
      map ("  " ++) ["; --- OP_RIGHT ---", "add rbx, " ++ show n]
    (OP_LEFT n, _) -> map ("  " ++) ["; --- OP_LEFT ---", "sub rbx, " ++ show n]
    (OP_JMP_IF_NULL, idx) ->
      "; --- OP_JMP_IF_NULL ---" :
      (".L" ++ show idx ++ ":") :
      map
        ("  " ++)
        ["cmp byte [rbx], 0", "jz .L" ++ show (fromJust $ jmp !? idx)]
    (OP_JMP_BACK_IF_NON_NULL, idx) ->
      "; --- OP_JMP_BACK_IF_NON_NULL ---" :
      (".L" ++ show idx ++ ":") :
      map
        ("  " ++)
        ["cmp byte [rbx], 0", "jnz .L" ++ show (fromJust $ jmp !? idx)]
    (OP_PRINT, _) ->
      map
        ("  " ++)
        [ "; --- OP_PRINT ---"
        , "mov rax, SYS_WRITE"
        , "mov rdi, STDOUT"
        , "mov rsi, rbx"
        , "mov rdx, 1"
        , "syscall"
        ]
    (OP_INPUT, _) ->
      map
        ("  " ++)
        [ "; --- OP_INPUT ---"
        , "mov rax, SYS_READ"
        , "mov rdi, STDIN"
        , "mov rsi, rbx"
        , "mov rdx, 1"
        , "syscall"
        ]
    (OP_SET_TO_NULL, _) ->
      map ("  " ++) ["; --- OP_SET_TO_NULL ---", "mov byte [rbx], 0"]
    (OP_EXIT, _) -> [""] -- We will actually ignore this one in asm (for now at least)
    (OP_MULT (offset, factor), _) ->
      map ("  " ++) $
      "; --- OP_MULT ---" :
      case compare offset 0 of
        GT ->
          [ "mov rax, " ++ show factor
          , "mul byte [rbx]"
          , "add [rbx + " ++ show offset ++ "], rax"
          ]
        LT ->
          [ "mov rax, " ++ show factor
          , "mul byte [rbx]"
          , "add [rbx - " ++ show (-offset) ++ "], rax"
          ]
        EQ -> error "Error in OP_MULT, offset should never be null!"

tapeSize :: Integer
tapeSize = 32768

header :: [String]
header =
  [ "BITS 64"
  , "%define SYS_EXIT 60"
  , "%define SYS_READ 0"
  , "%define SYS_WRITE 1"
  , "%define EXIT_SUCCESS 0"
  , "%define STDIN 0"
  , "%define STDOUT 1"
  , "global _start"
  , "section .text"
  , "_start:"
  , "  mov rbx, tape"
  ]

footer :: [String]
footer =
  [ "; --- PRINT_NEWLINE ---"
  , "push 10"
  , "mov rax, SYS_WRITE"
  , "mov rdi, STDOUT"
  , "mov rsi, rsp"
  , "mov rdx, 1"
  , "syscall"
  , "add rsp, 8"
  ] ++
  exitCall ++ ["section .data", "tape db " ++ show tapeSize ++ " dup(0)"]

exitCall :: [String]
exitCall =
  [ "; ------ EXIT ------"
  , "  mov rax, SYS_EXIT"
  , "  mov rdi, EXIT_SUCCESS"
  , "  syscall"
  ]
