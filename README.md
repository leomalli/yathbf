# YATHBF: Yet Another Toolset (in) Haskell (for) BrainFuck


This toy project is reinventing the wheel by implementing some common tools for working with [BrainFuck](https://en.wikipedia.org/wiki/Brainfuck).

One funny fact is that, using Haskell, you can transpile BrainFuck back into Haskell!


# Using the Library

In order to compile the tools, you must have the `cabal` (>= 2.4) tool installed -- which should come with all the necessary Haskell toolsets, e.g., `ghc`.

Build everything using:

    cabal v2-build all

The tools included can then be called using `cabal v2-run bfxxx -- <args>`.
For instance, interpreting the file `/path/to/hello-world.bf`, you would use:

    cabal v2-run bf-interpreter -- /path/to/hello-world.bf
    
Note that the transpilers takes two arguments, the source file, and the file to transpile to, e.g.,

    cabal v2-run bf2asm -- /path/to/hello-world.bf ./transpiled-hello-world.asm
    
transpile `/path/to/hello-world.bf` into NASM assambly in the file `./transpiled-hello-world.asm`.
You then have to compile it yourself to run it.
For instance:

    nasm -felf64 -g ./transpiled-hello-world.asm -o hello.o && ld hello.o -o hello && ./hello

compiles, links, and run the resulting executable.


To export the executables to your `cabal` path, you can execute `cabal v2-install all`

# RoadMap

- [X] Interpreter (named: `bf-interpreter`)
- [X] Transpiling to `C`(named: `bf2c`)
- [X] Transpiling to `Haskell`(named: `bf2hs`)
  - [ ] Implement the `Mult` optimization instruction -- eventually.
- [X] Transpiling to `x86_64` assambly (named: `bf2asm`) (POSIX only, with NASM syntax)
- [X] Basic optimizations (Compressing operations, recognising reset and multiply loops)
- [ ] Compiling directly to ELF64 executable (don't know yet if possible at all, will see)


# Notes

The interpreter is *really* slow...
This was expected as it is (poorly) written (and using Haskell), but still.
Last run of the famous `mandelbrot.bf` file took longer than I want to admit on my laptop...

I think it boils down to my poor usage of the `State Monad` construction, maybe I will come back and try to optimize it a bit one day.

Also note that the transpiled programs have a tendency to crash when transpiling `mandelbrot.bf`, I cannot really understand if my implementation is at fault or not.
The `bf2asm` output simply segfaults when the optimization `multLoops` is used, and I noticed that the `bf2c` output relies on undefined behaviour to function on the same optimization level.
As I noticed other implementations have difficulties with the `mandelbrot.bf` program, I decided to just ignore it and not loose my time on this minor problem.
