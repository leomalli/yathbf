{-# LANGUAGE LambdaCase #-}

module Optimizer
  ( fuseOPs
  , resetLoops
  , multiplicationLoops
  ) where

import Data.Function (on)
import Data.List (groupBy, sortBy)
import MyTypes
  ( BFInterRepr
  , OP_TYPE(..)
  , collapseOP
  , isCollapsable
  , isSameOPType
  )

fuseOPs :: BFInterRepr -> BFInterRepr
fuseOPs = concatMap collapseOP . groupBy isSameOPType

resetLoops :: BFInterRepr -> BFInterRepr
resetLoops (OP_JMP_IF_NULL:OP_DECR _:OP_JMP_BACK_IF_NON_NULL:xs) =
  OP_SET_TO_NULL : resetLoops xs
resetLoops (OP_JMP_IF_NULL:OP_INCR _:OP_JMP_BACK_IF_NON_NULL:xs) =
  OP_SET_TO_NULL : resetLoops xs
resetLoops (x:xs) = x : resetLoops xs
resetLoops [] = []

multiplicationLoops :: BFInterRepr -> BFInterRepr
multiplicationLoops ops = concat splited
  where
    fst3 (a, _, _) = a
    candidates = getInnerLoops 0 ops
    mults = filter (isMultLoop . snd) candidates
    mOps =
      sortBy
        (flip compare `on` fst3)
        [ (i, l + 2, mulOPs)
        | (i, xs) <- mults
        , let l = length xs
        , let mulOPs = multParamsToMultOP $ extractMultParams xs
        ]
    splited = myConvulotedSplitAndReplace mOps ops

myConvulotedSplitAndReplace ::
     [(Int, Int, [OP_TYPE])] -> [OP_TYPE] -> [[OP_TYPE]]
myConvulotedSplitAndReplace [] xs = [xs]
myConvulotedSplitAndReplace ((idx, l, ms):xs) origOP =
  myConvulotedSplitAndReplace xs beg ++ ms : [remains]
  where
    (beg, end) = splitAt idx origOP
    remains = drop l end

multParamsToMultOP :: [(Int, Int)] -> [OP_TYPE]
multParamsToMultOP [] = [OP_SET_TO_NULL]
multParamsToMultOP ((offset, factor):xs) =
  OP_MULT (offset, factor) : multParamsToMultOP xs

-- map extractMultParams $ filter isMultLoop $ getInnerLoops prog'
extractMultParams :: [OP_TYPE] -> [(Int, Int)]
extractMultParams =
  scanl1 (\(a1, _) (b1, b2) -> (a1 + b1, b2)) . extractRelativeMultParams

extractRelativeMultParams :: [OP_TYPE] -> [(Int, Int)]
extractRelativeMultParams [] = []
extractRelativeMultParams ops@(x:xs)
  | x == OP_DECR 1 = extractRelativeMultParams xs
  | last ops == OP_DECR 1 = extractRelativeMultParams cleanOP
  | null xs = []
  where
    cleanOP =
      case reverse ops of
        (_:xs') -> reverse xs'
        [] -> error "This should not be empty"
extractRelativeMultParams (movement:increment:xs) =
  (offset, factor) : extractRelativeMultParams xs
  where
    offset =
      case movement of
        OP_RIGHT n -> n
        OP_LEFT n -> -n
        _Any -> error "OP isn't pointer movement, should not be reachable"
    factor =
      case increment of
        OP_INCR n -> n
        OP_DECR n -> -n
        _Any -> error "OP isn't add/sub , should not be reachable"
extractRelativeMultParams _ = error "This should not be reachable"

isMultLoop :: [OP_TYPE] -> Bool
isMultLoop [] = False
isMultLoop ops@(x:_) =
  (isFirstOPDecr || isLastOPDecr) &&
  (sameLeftRight == 0) &&
  evenOPCount && not wrongOPTypes && onlyTwoDirectionBlocks
  where
    onlyTwoDirectionBlocks -- True: [>+>-<<] False: [>+><>-<<]
     = (== 2) $ length $ groupBy isSameOPType $ filter isLeftRight ops
    wrongOPTypes = not (all isCollapsable ops)
    sameLeftRight = getResultingMovement ops
    evenOPCount = even (length ops)
    isFirstOPDecr =
      case x of
        OP_DECR 1 -> True
        _Any -> False
    isLastOPDecr =
      case last ops of
        OP_DECR 1 -> True
        _Any -> False

isLeftRight :: OP_TYPE -> Bool
isLeftRight =
  \case
    OP_RIGHT _ -> True
    OP_LEFT _ -> True
    _Any -> False

-- Get the sum of the movements in the code segment, e.g. ">><>>" -> 3, "<<>><" -> -1
getResultingMovement :: [OP_TYPE] -> Int
getResultingMovement [] = 0
getResultingMovement (OP_RIGHT n:xs) = getResultingMovement xs + n
getResultingMovement (OP_LEFT n:xs) = getResultingMovement xs - n
getResultingMovement (_:xs) = getResultingMovement xs

-- This gets the inner loops suceptible of beeing optimized from the BF intermediate representation
getInnerLoops :: Int -> BFInterRepr -> [(Int, [OP_TYPE])]
getInnerLoops _ [] = []
getInnerLoops idx (OP_JMP_IF_NULL:xs) =
  if null v
    then getInnerLoops outIdx rest
    else (idx, v) : getInnerLoops outIdx rest
  where
    (v', rest, outIdx) = getInnerLoops' (idx + 1) xs
    v =
      if not (null v') && (last v' /= OP_JMP_IF_NULL)
        then v'
        else []
getInnerLoops idx (_:xs) = getInnerLoops (idx + 1) xs

getInnerLoops' :: Int -> [OP_TYPE] -> ([OP_TYPE], [OP_TYPE], Int)
getInnerLoops' _ [] = ([], [], -1)
getInnerLoops' idx (OP_JMP_BACK_IF_NON_NULL:xs) = ([], xs, idx + 1)
getInnerLoops' idx ops@(OP_JMP_IF_NULL:_) = ([OP_JMP_IF_NULL], ops, idx)
getInnerLoops' idx (x:xs) = (x : v, rest, outIdx)
  where
    (v, rest, outIdx) = getInnerLoops' (idx + 1) xs
