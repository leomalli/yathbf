module Interpreter
  ( createInterpreter
  , runInterpreter
  ) where

import MyTypes (BFInterRepr, OP_TYPE(..))

import Control.Monad.State (StateT(..), gets, lift, modify)
import Data.Char (chr, ord)
import Data.IntMap.Strict (IntMap, (!?))
import qualified Data.Sequence as S
import Data.Sequence (Seq(..))
import Data.Word (Word8)

-- import Debug.Trace
trace :: String -> a -> a
trace _ = id

createInterpreter :: BFInterRepr -> IntMap Int -> Interpreter
createInterpreter = initialInterpreterState

runInterpreter :: Interpreter -> IO ((), Interpreter)
runInterpreter i = do
  (s, r) <- stateMachine i stepInstructionS
  return (s, r)

-- Tape of the bf programm
tapeSize :: Int
tapeSize = 32768

data Tape = Tape
  { _seq :: !(Seq Word8)
  , _pt :: !Int
  }

lookCur :: Tape -> Word8
lookCur t = _seq t `S.index` _pt t

data Effect
  = None
  | Result !String
  | Exit
  deriving (Show)

data Interpreter = Interpreter
  { _tape :: !Tape -- Infinite object
  , _prog :: !BFInterRepr -- This should be finite
  , _ip :: !Int
  , _jmpMap :: !(IntMap Int)
  }

initialTape :: Tape
initialTape = Tape (S.replicate tapeSize 0) 0

setTo :: Word8 -> Tape -> Tape
setTo w t = t {_seq = S.adjust' (const w) (_pt t) (_seq t)}

tapeOp :: OP_TYPE -> Tape -> Tape
tapeOp op t =
  case op of
    OP_INCR n ->
      trace ("OP: INCR " ++ show n) $
      t {_seq = S.adjust' (\w -> w + fromIntegral n) (_pt t) (_seq t)}
    OP_DECR n ->
      trace ("OP: DECR " ++ show n) $
      t {_seq = S.adjust' (\w -> w - fromIntegral n) (_pt t) (_seq t)}
    OP_RIGHT n ->
      trace ("OP: RIGHT " ++ show n ++ "\tpt: " ++ show (_pt t)) $
      t {_pt = _pt t + n}
    OP_LEFT n ->
      trace ("OP: LEFT " ++ show n ++ "\tpt: " ++ show (_pt t)) $
      t {_pt = _pt t - n}
    OP_SET_TO_NULL ->
      trace ("OP: SET_TO_NULL\tpt: " ++ show (_pt t)) $
      t {_seq = S.adjust' (const 0) (_pt t) (_seq t)}
    OP_MULT (offset, factor) ->
      trace ("OP: MULT offset=" ++ show offset ++ " incr=" ++ show factor) $
      t
        { _seq =
            S.adjust'
              (\w -> w + lookCur t * fromIntegral factor)
              (offset + _pt t)
              (_seq t)
        }
    _Any -> error $ "Operation not handled: " ++ show _Any

type InterS a = StateT Interpreter IO a

stepInstructionS :: InterS Effect
stepInstructionS = do
  ip <- gets _ip
  tape <- gets _tape
  prog <- gets _prog
  -- Handle possible instructions
  case prog !! ip of
    OP_INCR n -> do
      modify $ \s -> s {_tape = tapeOp (OP_INCR n) tape, _ip = ip + 1}
      return None
    OP_DECR n -> do
      modify $ \s -> s {_tape = tapeOp (OP_DECR n) tape, _ip = ip + 1}
      return None
    OP_RIGHT n -> do
      modify $ \s -> s {_tape = tapeOp (OP_RIGHT n) tape, _ip = ip + 1}
      return None
    OP_LEFT n -> do
      modify $ \s -> s {_tape = tapeOp (OP_LEFT n) tape, _ip = ip + 1}
      return None
    OP_JMP_IF_NULL -> do
      case lookCur tape of
        0 -> do
          jmpMap <- gets _jmpMap
          ip' <-
            case jmpMap !? ip of
              Nothing -> error "Mismatched ["
              Just n -> pure n
          modify $ \s -> s {_ip = ip'}
          return None
        _Any -> do
          modify $ \s -> s {_ip = ip + 1}
          return None
    OP_JMP_BACK_IF_NON_NULL -> do
      case lookCur tape of
        0 -> do
          modify $ \s -> s {_ip = ip + 1}
          return None
        _Any -> do
          jmpMap <- gets _jmpMap
          ip' <-
            case jmpMap !? ip of
              Nothing -> error "Mismatched ]"
              Just n -> pure n
          modify $ \s -> s {_ip = ip'}
          return None
    OP_PRINT -> do
      modify $ \s -> s {_ip = ip + 1}
      let char = chr . fromIntegral $ lookCur tape
      lift $ putChar char
      return $ Result (show char)
    OP_INPUT -> do
      char <- fromIntegral . ord <$> lift getChar
      modify $ \s -> s {_ip = ip + 1, _tape = setTo char tape}
      return None
    OP_EXIT -> return Exit
    OP_SET_TO_NULL -> do
      modify $ \s -> s {_tape = tapeOp OP_SET_TO_NULL tape, _ip = ip + 1}
      return None
    OP_MULT (offset, incr) -> do
      modify $ \s ->
        s {_tape = tapeOp (OP_MULT (offset, incr)) tape, _ip = ip + 1}
      return None

stateMachine :: Interpreter -> InterS Effect -> IO ((), Interpreter)
stateMachine initS stepS
  -- r <- evalStateT (sequence . repeat $ stepInstructionS) interpreter
 = do
  (r, s) <- runStateT stepS initS
  case r of
    Exit -> return ((), s)
    _Any -> stateMachine s stepS

initialInterpreterState :: BFInterRepr -> IntMap Int -> Interpreter
initialInterpreterState code im =
  Interpreter {_tape = initialTape, _prog = code, _ip = 0, _jmpMap = im}
