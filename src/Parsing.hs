{-# LANGUAGE LambdaCase #-}

module Parsing
  ( parseToIR
  , createJmpMap
  ) where

import qualified Data.IntMap.Strict as IntMap
import Data.IntMap.Strict (IntMap)
import MyTypes (BFInterRepr, OP_TYPE(..))

charToOP :: Char -> OP_TYPE
charToOP =
  \case
    '+' -> OP_INCR 1
    '-' -> OP_DECR 1
    '>' -> OP_RIGHT 1
    '<' -> OP_LEFT 1
    '.' -> OP_PRINT
    '[' -> OP_JMP_IF_NULL
    ']' -> OP_JMP_BACK_IF_NON_NULL
    ',' -> OP_INPUT
    _Any -> error $ "Cannot determine corresponding OP to:\t" ++ [_Any]

isOP :: Char -> Bool
isOP c = c `elem` "+-<>.,[]"

parseToIR :: String -> BFInterRepr
parseToIR ss = ir ++ [OP_EXIT]
  where
    ir = map charToOP . filter isOP $ ss

-- Matches "["" to "]" in a very ineficient way
createJmpMap :: BFInterRepr -> IntMap Int
createJmpMap prog = createMapInternal [] IntMap.empty $ zip prog [0 ..]

createMapInternal :: [Int] -> IntMap Int -> [(OP_TYPE, Int)] -> IntMap Int
createMapInternal acc j [] =
  if null acc
    then j
    else error
           "Missmatching [] while parsing (Non-Empty Stack at the end of parsing)"
createMapInternal [] jmpMap (op:ops)
  | fst op == OP_JMP_IF_NULL = createMapInternal [snd op] jmpMap ops
  | fst op == OP_JMP_BACK_IF_NON_NULL =
    error "Missmatching [] while parsing (Empty Stack)"
  | otherwise = createMapInternal [] jmpMap ops
createMapInternal stack@(s:ss) jmpMap (op:ops)
  | fst op == OP_JMP_IF_NULL = createMapInternal (snd op : stack) jmpMap ops
  | fst op == OP_JMP_BACK_IF_NON_NULL = createMapInternal ss jmpMap' ops
  | otherwise = createMapInternal stack jmpMap ops
  where
    jmpMap' = IntMap.insert (snd op) s $ IntMap.insert s (snd op) jmpMap
