{-# LANGUAGE LambdaCase #-}

module MyTypes
  ( OP_TYPE(..)
  , BFInterRepr
  , isSameOPType
  , collapseOP
  , isCollapsable
  ) where

import Data.Foldable (foldl')

data OP_TYPE
  = OP_INCR !Int
  | OP_DECR !Int
  | OP_RIGHT !Int
  | OP_LEFT !Int
  | OP_PRINT
  | OP_JMP_IF_NULL
  | OP_JMP_BACK_IF_NON_NULL
  | OP_INPUT
  | OP_EXIT
  | OP_SET_TO_NULL
  | OP_MULT !(Int, Int)
  deriving (Show, Eq)

-- BrainFuck Intermediate Representation
type BFInterRepr = [OP_TYPE]

isSameOPType :: OP_TYPE -> OP_TYPE -> Bool
isSameOPType (OP_INCR _) (OP_INCR _) = True
isSameOPType (OP_DECR _) (OP_DECR _) = True
isSameOPType (OP_LEFT _) (OP_LEFT _) = True
isSameOPType (OP_RIGHT _) (OP_RIGHT _) = True
isSameOPType (OP_MULT _) (OP_MULT _) = True
isSameOPType OP_PRINT OP_PRINT = True
isSameOPType OP_INPUT OP_INPUT = True
isSameOPType OP_JMP_IF_NULL OP_JMP_IF_NULL = True
isSameOPType OP_JMP_BACK_IF_NON_NULL OP_JMP_BACK_IF_NON_NULL = True
isSameOPType OP_SET_TO_NULL OP_SET_TO_NULL = True
isSameOPType _ _ = False

adds :: OP_TYPE -> OP_TYPE -> OP_TYPE
adds (OP_INCR x) (OP_INCR y) = OP_INCR (x + y)
adds (OP_DECR x) (OP_DECR y) = OP_DECR (x + y)
adds (OP_LEFT x) (OP_LEFT y) = OP_LEFT (x + y)
adds (OP_RIGHT x) (OP_RIGHT y) = OP_RIGHT (x + y)
adds _ _ = error "Cannot add two uncollapsable OPs"

isCollapsable :: OP_TYPE -> Bool
isCollapsable =
  \case
    OP_INCR _ -> True
    OP_DECR _ -> True
    OP_RIGHT _ -> True
    OP_LEFT _ -> True
    _Any -> False

collapseOP :: [OP_TYPE] -> [OP_TYPE]
collapseOP [] = []
collapseOP ops@(x:xs)
  | not $ isCollapsable x = ops
  | otherwise = [foldl' adds x xs]
